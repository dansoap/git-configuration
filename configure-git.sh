#/bin/bash

# script to automatically configure git user/email
# see https://bitbucket.org/dansoap/git-configuration

if [ ! -x /usr/bin/git ]; then
	exit 0;
fi

USER=`git config -l | egrep 'user.name' | cut -d'=' -f2`
EMAIL=`git config -l | egrep 'user.email' | cut -d'=' -f2`

if [ -n "$USER" ] && [ -n "$EMAIL" ]; then
	exit 0;
fi

if [ -z "$USER" ]; then
	echo -n "No name set yet, please enter your name: "
	read NEWUSER
else
	NEWUSER=$USER
fi 

if [ -z "$EMAIL" ]; then
	echo -n "No email set yet, please enter your email: "
	read NEWEMAIL
else
	NEWEMAIL=$EMAIL
fi


echo "The following will be set in git:"
echo ""
echo "user.name="$NEWUSER
echo "user.email="$NEWEMAIL
echo ""
echo -n "Is this correct [Y/n]? "
read ANSWER
if [ -z "$ANSWER" ] || [ "$ANSWER" = "Y" ] || [ "$ANSWER" = "y" ]; then

	git config --global user.name "$NEWUSER"
	git config --global user.email "$NEWEMAIL"
	echo "Done"
else
	echo "Aborting"
fi
